package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.eventloop.opmode.TeleOp;

import org.firstinspires.ftc.teamcode.Walbots7175.internal.NewOpMode;


/**
 * Created by nicolas on 1/28/18 in ftc_app.
 * <p>
 * Copyright (c) ©2018 Nicolas Hohaus
 * Copyright (c) ©2018 Walbots (7175)
 * <p>
 * Resource: https://gitlab.com/roboticsclub/ftc_app
 * Contact: nico@walbots.com, team@walbots.com
 */


/**
 * The class UserControlledMode runs all user controlled actions that are triggered by the keys
 * on two gamepads. To enable a variety of different actions and functionality to the drivers
 * other classes are instantiated and called continually that ensure the the whole functionality
 * is available and the drivers can control the whole robot.
 */
@TeleOp(name="User Controlled Sample", group="Competition")
public class UserControlledMode extends NewOpMode
{

    //TODO: Define all controllers for UserControlledMode

    /**
     * The init() method is part of the OpMode life-cycle and gets called once when the initialize
     * button is pressed on the phone.
     *
     * In this case we will initialize the hardware and the various controllers that will enable
     * the user to control the robot.
     *
     * @param initialize This boolean will always be true, you can ignore it
     * @return Whether the OpMode initialized successfully
     */
    @Override
    public boolean init(boolean initialize)
    {
        //TODO: Set all controllers for UserControlledMode

        return true;    //Successfully initialized!
    }


    /**
     * The loop() method is part of the OpMode life-cycle and gets called repeatedly after the play
     * button is pressed on the phone.
     *
     * In this case all controllers are called so they can read the input from the gamepad and react
     * with various actions the robot can perform.
     *
     * So that we do not have to worry about all possible robot actions in this place, those actions
     * are divided up into separate controllers and their run() methods. With that we only have to
     * worry about specific actions and functionality in their controllers instead of about
     * everything at once here.
     */
    @Override
    public void loop()
    {
        //TODO: Call all controllers for UserControlledMode
    }
}
